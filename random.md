# Misc offsets/values

## Character Type

| Offset | Value  | Meaning                   |
| ------ | ------ | ------------------------- |
| 0x004e | 0b0000 | Character is a Berserker  |
| 0x00ce | 0b0010 | Character is a Troubadour |
| 0x014e | 0b0011 | Character is an Assassin  |
| 0x01ce | 0b0001 | Character is a Runemaster |

## Horses

Stored in offset 0x0250

| Value      | Horses    |
| ---------- | --------- |
| 0b00000000 | None      |
| 0b00000001 | Nags      |
| 0b00000010 | Chargers  |
| 0b00000011 | Destriers |

## Selected character

Stored in offset 0x0274-0x0275

| Value      | Character  |
| ---------- | ---------- |
| 0b00000000 | Berserker  |
| 0b00000001 | Troubadour |
| 0b00000010 | Assassin   |
| 0b00000011 | Runemaster |

## Time/date

0x276-279 (4 bytes)

No idea what this represents yet.

## Keeps men

| Offset      | Keep      |
| ----------- | --------- |
| 0x0394-0395 | Delmortis |
| 0x039A-039B | Hightower |
| 0x03A6-03A7 | Eb's Pass |
| 0x03AE-03Af | Boot Hill |

## Names of things

Starting at 0x587A is a list of names for items/people. The format consists of 1 byte for the length and then the name of the item. There appears to be a length limit of 0xC (12) characters for each name, so some names are split across multiple entries (for example, `Warriors Awaking` is more than 12, so there is an entry for `Warriors` and another for `Awaking`).

| Item                                |
| ----------------------------------- |
| Forward spell                       |
| Surround spell                      |
| Missile spell                       |
| Continuous spell                    |
| Damage spell                        |
| Healing spell                       |
| Dispell spell                       |
| Speed spell                         |
| Paralyze spell                      |
| Antimage spell                      |
| Make Weapon spell                   |
| Teleport spell                      |
| Regeneration spell                  |
| Disrupt spell                       |
| Wing of Bat ingredient              |
| Brimstone ingredient                |
| Mandrake ingredient                 |
| Crystal ingredient                  |
| Hedjog Venom ingredient             |
| Dragon Tooth ingredient             |
| Nightshade ingredient               |
| Phoenix Claw ingredient             |
| Berserker first name                |
| Berserker last name                 |
| Troubadour first name               |
| Troubadour last name                |
| Assassin first name                 |
| Assassin last name                  |
| Runemaster first name               |
| Runemaster last name                |
| Berserker home town (padded to 12)  |
| Troubadour home town (padded to 12) |
| Assassin home town (padded to 12)   |
| Runemaster home town (padded to 12) |
| of (word)                           |
| bashes (word)                       |
| smashes (word)                      |
| slashes (word)                      |
| swipes (word)                       |
| chops (word)                        |
| jabs (word)                         |
| stabs (word)                        |
| slices (word)                       |
| dodge (word)                        |
| ward (word)                         |
| block (word)                        |
| parry (word)                        |
| luck (word)                         |
| does (word)                         |
| a (word)                            |
| for (word)                          |
| just ran (word)                     |
| out (word)                          |
| blasts (word)                       |
| takes (word)                        |
| some (word)                         |
| sees (word)                         |
| nothing (word)                      |
| there (word)                        |
| cannot (word)                       |
| carry (word)                        |
| more (word)                         |
| finds (word)                        |
| no (word)                           |
| treasure (word)                     |
| monsters (word)                     |
| Monster (word)                      |
| Wilderness (word)                   |
| Leercote (word)                     |
| Zorendorf (word)                    |
| Delmortis (word)                    |
| Creyndor (word)                     |
| Martindale (word)                   |
| Hightower (word)                    |
| Smathost (word)                     |
| Treihadwyl (word)                   |
| Goriah (word)                       |
| Pongbarn (word)                     |
| Hill's End (word)                   |
| Eb's Pass (word)                    |
| Groghurst (word)                    |
| Balenhalm (word)                    |
| Titanshal (word)                    |
| Boot Hill (word)                    |
| Dak's Pond (word)                   |
| Brodfird (word)                     |
| The King (word)                     |
| Fagranc (word)                      |
| MoonHenge (word)                    |
| The Dark Tower (word)               |
| The Ancient (word)                  |
| The UnShrine (word)                 |
| The Guild (word)                    |
| Blacksmith (word)                   |
| Apothecary (word)                   |
| Holy Temple (word)                  |
| Tavern (word)                       |
| Artificer (word)                    |
| Enemy Banner (word)                 |
| No Credit! (word)                   |
| - (word)                            |
| Moondy (word)                       |
| Chewsday (word)                     |
| Hisday (word)                       |
| Hersday (word)                      |
| Farsdy (word)                       |
| Sadsdy (word)                       |
| Beamday (word)                      |
| Djinuary (word)                     |
| Fevery (word)                       |
| Mage (word)                         |
| Ample (word)                        |
| Maybe (word)                        |
| Join (word)                         |
| Jewel (word)                        |
| Hagcursed (word)                    |
| Cryptunder (word)                   |
| Orctogre (word)                     |
| Newvendor (word)                    |
| Drakkender (word)                   |
| the (word)                          |
| st (word)                           |
| nd (word)                           |
| rd (word)                           |
| th (word)                           |
| is (word)                           |
| under (word)                        |
| siege (word)                        |
| Evil (word)                         |
| attack (word)                       |
| falls (word)                        |
| to (word)                           |
| destroys (word)                     |
| hoardes (word)                      |
| Barely (word)                       |
| Lightly (word)                      |
| Well (word)                         |
| Heavily (word)                      |
| Defended (word)                     |
| Enemy (word)                        |
| Occupied (word)                     |
| The Thief of (word)                 |
| Dolik Pass (word)                   |
| Warriors (word)                     |
| Awaking (word)                      |
| The Ballad (word)                   |
| Eleanor (word)                      |
| Kijam's (word)                      |
| Litany (word)                       |
| The Smithie (word)                  |
| Song (word)                         |
| March of the (word)                 |
| Bold Ones (word)                    |
| Adieu Sweet (word)                  |
| Dullard (word)                      |
| Dance of the (word)                 |
| Faerie Queen (word)                 |
| dexterity (word)                    |
| strength (word)                     |
| dodge ability (word)                |
| constitution (word)                 |
| armour class (word)                 |
| healing rate (word)                 |
| intelligence (word)                 |
| speed (word)                        |
| Berserker (word)                    |
| Troubadour (word)                   |
| Assassin (word)                     |
| Runemaster (word)                   |
| You are approaching (word)          |
| The men (word)                      |
| reach (word)                        |
| are (word)                          |
| repulsed (word)                     |
| liberated (word)                    |
| reinforced (word)                   |
| Buy Nags (word)                     |
| Buy Chargers (word)                 |
| Buy Destriers (word)                |
| None (word)                         |
| Nag (word)                          |
| Charger (word)                      |
| Destrier (word)                     |
| The door (word)                     |
| locked with (word)                  |
| will not budge (word)               |
| Dagger (word)                       |
| Mithril (word)                      |
| Dark (word)                         |
| Death (word)                        |
| Diamond (word)                      |
| Short (word)                        |
| Sword (word)                        |
| Blade (word)                        |
| Stealth (word)                      |
| Broad (word)                        |
| Vorpal (word)                       |
| Arcane (word)                       |
| Heron (word)                        |
| Doom (word)                         |
| Battle (word)                       |
| Axe (word)                          |
| Blood (word)                        |
| Winged (word)                       |
| Mystic (word)                       |
| Elf (word)                          |
| Frost (word)                        |
| Ancient (word)                      |
| Slugger (word)                      |
| Permit (word)                       |
| Staff (word)                        |
| Moon (word)                         |
| Dragon (word)                       |
| Sun (word)                          |
| Serpent (word)                      |
| Cloud (word)                        |
| Amber (word)                        |
| Mail (word)                         |
| Chain (word)                        |
| Plate (word)                        |
| Crystal (word)                      |
| Bracers (word)                      |
| Gauntlets (word)                    |
| Leather (word)                      |
| Chaos (word)                        |
| Gloves (word)                       |
| Buckler (word)                      |
| Shield (word)                       |
| Arc (word)                          |
| War (word)                          |
| Bane (word)                         |
| Leathers (word)                     |
| Helm (word)                         |
| Hero (word)                         |
| Golden (word)                       |
| Boots (word)                        |
| Iron (word)                         |
| Ring (word)                         |
| Amulet (word)                       |
| Wand (word)                         |
| Potion (word)                       |
| Shadow (word)                       |
| Scroll (word)                       |
| Gold (word)                         |
| Bag of (word)                       |
| Key (word)                          |
| Silver (word)                       |
| Bronze (word)                       |
| Skull (word)                        |
| Azure (word)                        |
| Emerald (word)                      |
| Ruby (word)                         |
| Topaz (word)                        |
| Ornate (word)                       |
| Lyre (word)                         |
| Harmonic (word)                     |
| Angel (word)                        |
| Harp (word)                         |
| Horn (word)                         |
| Mandolin (word)                     |
| Figurine (word)                     |
| Holy (word)                         |
| Relic (word)                        |
| Craven (word)                       |
| Image (word)                        |
| Mixing (word)                       |
| Bowl (word)                         |
| Power (word)                        |
| UnKey (word)                        |
| Blank83 (word)                      |
| Blank84 (word)                      |
| bat wings (word)                    |
| sulphur (word)                      |
| roots (word)                        |
| crystals (word)                     |
| venom sacs (word)                   |
| ugly teeth (word)                   |
| berries (word)                      |
| fiery claws (word)                  |
| Corpse (word)                       |
| Robes (word)                        |
| Berserker first name plural         |
| Troubadour first name plural        |
| Assassin first name plural          |
| Runemaster first name plural        |
