# Runecaster

## Character offset

0x0184

## Item locations

Positions are from the top-centre of the screen being 01 01

| Offset      | Use                            |
| ----------- | ------------------------------ |
| 0x0184      | East-West position on screen   |
| 0x0185      | North-South position on screen |
| 0x0186      | Move to position               |
| 0x0186      | Move to position               |
| 0x0194      | AC modifier                    |
| 0x0195      |                                |
| 0x0196      | Str                            |
| 0x0197      |                                |
| 0x0198      | Int                            |
| 0x0199      |                                |
| 0x019A      | Spd                            |
| 0x019B      |                                |
| 0x019C      | Dex                            |
| 0x019D      |                                |
| 0x019E      | Con                            |
| 0x019F      |                                |
| 0x01A0      | Luck                           |
| 0x01A1      |                                |
| 0x01A2      |                                |
| 0x01a6-01a7 | Remaining HP                   |
| 0x01a8-01a9 | Total HP                       |
| 0x01aa      | First item slot                |
| 0x01b9      | Last item slot                 |
| 0x01ba      | Slot of equipped helm          |
| 0x01bb      | Slot of equipped ?             |
| 0x01bc      | Slot of equipped footware      |
| 0x01bd      | Slot of equipped shield        |
| 0x01be      | Slot of equipped armour        |
| 0x01bf      | Slot of equipped gloves        |
| 0x01c0      | Slot of equipped weapon        |
| 0x01c1      | Slot of equipped wand/scroll   |
| 0x01c2      | Slot of equipped off-hand item |
| 0x01c3      | Slot of selected item          |
| 0x01c4      |                                |
| 0x01c5      | Level                          |
| 0x01c6-01c9 | Gold                           |
| 0x01ca      | XP                             |
| 0x01cb      | XP                             |
| 0x01cc      | XP                             |
| 0x01cd      | XP                             |
| 0x01ce      | XP                             |
| 0x01cf      | XP                             |
| 0x0204-0205 | Quantity of Phoenix Claw       |
| 0x0206-0207 | Quantity of Nightshade         |
| 0x0208-0209 | Quantity of Dragon Tooth       |
| 0x020A-020B | Quantity of Hedjog Venom       |
| 0x020C-020D | Quantity of Crystal            |
| 0x020E-020F | Quantity of Mandrake           |
| 0x0210-0211 | Quantity of Brimstone          |
| 0x0212-0213 | Quantity of Wing of Bat        |

## Spells

| Mask (0x286) | Spell        |
| ------------ | ------------ |
| 0b00000001   | Paralyze     |
| 0b00000010   | Antimage     |
| 0b00000100   | Thrall       |
| 0b00001000   | Make Weapon  |
| 0b00010000   | Teleport     |
| 0b00100000   | Regeneration |
| 0b01000000   | Disrupt      |
| 0b10000000   | Vivify       |

| Mask (0x287) | Spell      |
| ------------ | ---------- |
| 0b00000001   | Forward    |
| 0b00000010   | Surround   |
| 0b00000100   | Missile    |
| 0b00001000   | Continuous |
| 0b00010000   | Damage     |
| 0b00100000   | Healing    |
| 0b01000000   | Dispell    |
| 0b10000000   | Speed      |

| Offset | Use                        |
| ------ | -------------------------- |
| 0x4856 | Number of spells in slot 1 |
| 0x4857 | Number of spells in slot 2 |
| 0x4858 | Number of spells in slot 3 |
| 0x4859 | Number of spells in slot 4 |
| 0x485A | Number of spells in slot 5 |
| 0x485B | Number of spells in slot 6 |
| 0x485C | Number of spells in slot 7 |
| 0x485D | Number of spells in slot 8 |
| 0x485E | Number of spells in slot 9 |
| 0x485F | Number of spells in slot 0 |

