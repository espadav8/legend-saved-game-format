# Assassin

## Character offset

0x0104

## Item locations

| Offset      | Use             |
| ----------- | --------------- |
| 0x012a      | First item slot |
| 0x0139      | Last item slot  |
| 0x0146-0149 | Gold            |
