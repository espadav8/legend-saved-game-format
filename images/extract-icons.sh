#!/bin/bash

input=${1}
output='icons'
icon=0

start_x=64
start_y=272
spacing_x=14
spacing_y=14
size_x=34
size_y=34
background='srgb(68,34,0)'

rm -v icon-*.png

if [ -z "${input}" ]; then
    echo "Must give a file name"
    exit 1
fi

for i in $(seq 0 15);
do
    offset_x=$((start_x + (((size_x + spacing_x) * (i % 8)))))
    offset_y=$((start_y + (((size_y + spacing_y) * (i >> 3)))))
    output="icon-${i}"

    magick "${input}" \
        -crop "${size_x}x${size_y}+${offset_x}+${offset_y}" \
        -bordercolor "${background}" \
        -border 1x1 \
        -fill transparent \
        -draw 'color 0,0 floodfill' \
        -shave 1x1 \
        "${output}.png"

    echo
done
