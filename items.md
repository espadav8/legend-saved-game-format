# Items

## Working items

| Value | Item         | Image                                               |
| ----- | ------------ | --------------------------------------------------- |
| 0x00  | Empty        |                                                     |
| 0x01  | Helm         | ![Helm](./images/helms/01-helm.png)                 |
| 0x02  | Golden Helm  | ![Golden Helm](./images/helms/02-helm-golden.png)   |
| 0x03  | Frost Helm   | ![Frost Helm](./images/helms/03-helm-frost.png)     |
| 0x04  | Mithril Helm | ![Mithril Helm](./images/helms/04-helm-mithril.png) |
| 0x05  | Blood Helm   | ![Blood Helm](./images/helms/05-helm-blood.png)     |
| 0x06  | Cloud Helm   | ![Cloud Helm](./images/helms/06-helm-cloud.png)     |
| 0x07  | Angel Helm   | ![Angel Helm](./images/helms/07-helm-angel.png)     |
| 0x08  | Dragon Helm  | ![Dragon Helm](./images/helms/08-helm-dragon.png)   |
| 0x09  | Holy Helm    | ![Holy Helm](./images/helms/09-helm-holy.png)       |
| 0x0A  | Winged Helm  | ![Winged Helm](./images/helms/0A-helm-winged.png)   |
| 0x0B  | Serpent Helm | ![Serpent Helm](./images/helms/0B-helm-serpent.png) |
| 0x0C  | Hero Helm    | ![Hero Helm](./images/helms/0C-helm-hero.png)       |
| 0x0D  | War Helm     | ![War Helm](./images/helms/0D-helm-war.png)         |
| 0x0E  | Doom Helm    | ![Doom Helm](./images/helms/0E-helm-doom.png)       |
| 0x0F  | Chaos Helm   | ![Chaos Helm](./images/helms/0F-helm-chaos.png)     |

| Value | Item            | Image                                                              |
| ----- | --------------- | ------------------------------------------------------------------ |
| 0x10  | Death Helm      | ![Death Helm](./images/helms/10-helm-death.png)                    |
| 0x11  | Sun Amulet      | ![Sun Amulet](./images/amulets/11-amulet-sun.png)                  |
| 0x12  | Moon Amulet     | ![Moon Amulet](./images/amulets/12-amulet-moon.png)                |
| 0x13  | Serpent Amulet  | ![Serpent Amulet](./images/amulets/13-amulet-serpent.png)          |
| 0x14  | Amber Amulet    | ![Amber Amulet](./images/amulets/14-amulet-amber.png)              |
| 0x15  | Dragon Amulet   | ![Dragon Amulet](./images/amulets/15-amulet-dragon.png)            |
| 0x16  | Cloud Amulet    | ![Cloud Amulet](./images/amulets/16-amulet-cloud.png)              |
| 0x17  | Leather boots   | ![Leather boots](./images/boots/17-boots-leather.png)              |
| 0x18  | Iron boots      | ![Iron boots](./images/boots/18-boots-iron.png)                    |
| 0x19  | Elf boots       | ![Elf boots](./images/boots/19-boots-elf.png)                      |
| 0x1A  | Golden boots    | ![Golden boots](./images/boots/1A-boots-golden.png)                |
| 0x1B  | Crystal boots   | ![Crystal boots](./images/boots/1B-boots-crystal.png)              |
| 0x1C  | Cloud boots     | ![Cloud boots](./images/boots/1C-boots-cloud.png)                  |
| 0x1D  | Leather Buckler | ![Leather Buckler](./images/shields/1D-shield-leather-buckler.png) |
| 0x1E  | Iron Buckler    | ![Iron Buckler](./images/shields/1E-shield-iron-buckler.png)       |
| 0x1F  | Arc Shield      | ![Arc Shield](./images/shields/1F-shield-arc.png)                  |

| Value | Item           | Image                                                            |
| ----- | -------------- | ---------------------------------------------------------------- |
| 0x20  | Battle Bane    | ![Battle Bane](./images/shields/20-shield-battle-bane.png)       |
| 0x21  | Iron Shield    | ![Iron Shield](./images/shields/21-shield-iron.png)              |
| 0x22  | Golden Shield  | ![Golden Shield](./images/shields/22-shield-golden.png)          |
| 0x23  | Bane Shield    | ![Bane Shield](./images/shields/23-shield-bane.png)              |
| 0x24  | Dragon Shield  | ![Dragon Shield](./images/shields/24-shield-dragon.png)          |
| 0x25  | War Shield     | ![War Shield](./images/shields/25-shield-war.png)                |
| 0x26  | Serpent Shield | ![Serpent Shield](./images/shields/26-shield-serpent.png)        |
| 0x27  | Heron Shield   | ![Heron Shield](./images/shields/27-shield-heron.png)            |
| 0x28  | Robes          | ![Robes](./images/armours/28-armour-robes.png)                   |
| 0x29  | Leathers       | ![Leathers](./images/armours/29-armour-leathers.png)             |
| 0x2A  | Chain Mail     | ![Chain Mail](./images/armours/2A-armour-chain-mail.png)         |
| 0x2B  | Plate Mail     | ![Plate Mail](./images/armours/2B-armour-plate-mail.png)         |
| 0x2C  | Bracers        | ![Bracers](./images/armours/2C-armour-bracers.png)               |
| 0x2D  | Arcane Bracers | ![Arcane Bracers](./images/armours/2D-armour-arcane-bracers.png) |
| 0x2E  | Mithril Chain  | ![Mithril Chain](./images/armours/2E-armour-mithril-chain.png)   |
| 0x2F  | Mithril Plate  | ![Mithril Plate](./images/armours/2F-armour-mithril-plate.png)   |

| Value | Item            | Image                                                              |
| ----- | --------------- | ------------------------------------------------------------------ |
| 0x30  | Stealth Bracers | ![Stealth Bracers](./images/armours/30-armour-stealth-bracers.png) |
| 0x31  | Blood Leathers  | ![Blood Leathers](./images/armours/31-armour-blood-leathers.png)   |
| 0x32  | Crystal Chain   | ![Crystal Chain](./images/armours/32-armour-crystal-chain.png)     |
| 0x33  | Crystal Plate   | ![Crystal Plate](./images/armours/33-armour-crystal-plate.png)     |
| 0x34  | Holy Crystal    | ![Holy Crystal](./images/armours/34-armour-holy-crystal.png)       |
| 0x35  | Leather Gloves  | ![Leather Gloves](./images/gloves/35-gloves-leather.png)           |
| 0x36  | Amber Ring      | ![Amber Ring](./images/rings/36-ring-amber.png)                    |
| 0x37  | Gauntlets       | ![Gauntlets](./images/gloves/37-gloves-gauntlets.png)              |
| 0x38  | Chaos Gloves    | ![Chaos Gloves](./images/gloves/38-gloves-chaos.png)               |
| 0x39  | Serpent Ring    | ![Serpent Ring](./images/rings/39-ring-serpent.png)                |
| 0x3A  | Cloud Ring      | ![Cloud Ring](./images/rings/3A-ring-cloud.png)                    |
| 0x3B  | Dragon Ring     | ![Dragon Ring](./images/rings/3B-ring-dragon.png)                  |
| 0x3C  | Blood Ring      | ![Blood Ring](./images/rings/3C-ring-blood.png)                    |
| 0x3D  | Stealth Gloves  | ![Stealth Gloves](./images/gloves/3D-gloves-stealth.png)           |
| 0x3E  | Sun Ring        | ![Sun Ring](./images/rings/3E-ring-sun.png)                        |
| 0x3F  | Moon Ring       | ![Moon Ring](./images/rings/3F-ring-moon.png)                      |

| Value | Item           | Image                                                            |
| ----- | -------------- | ---------------------------------------------------------------- |
| 0x40  | Mithril Gloves | ![Mithril Gloves](./images/gloves/40-gloves-mithril.png)         |
| 0x41  | Battle Gloves  | ![Battle Gloves](./images/gloves/41-gloves-battle.png)           |
| 0x42  | Dagger         | ![Dagger](./images/weapons/42-weapon-dagger.png)                 |
| 0x43  | Short Sword    | ![Short Sword](./images/weapons/43-weapon-short-sword.png)       |
| 0x44  | Staff          | ![Staff](./images/weapons/44-weapon-staff.png)                   |
| 0x45  | Broad Sword    | ![Broad Sword](./images/weapons/45-weapon-broad-sword.png)       |
| 0x46  | Mithril Dagger | ![Mithril Dagger](./images/weapons/46-weapon-mithril-dagger.png) |
| 0x47  | Battle Staff   | ![Battle Staff](./images/weapons/47-weapon-battle-staff.png)     |
| 0x48  | Battle Axe     | ![Battle Axe](./images/weapons/48-weapon-battle-axe.png)         |
| 0x49  | Mithril Blade  | ![Mithril Blade](./images/weapons/49-weapon-mithril-blade.png)   |
| 0x4A  | Mithril Sword  | ![Mithril Sword](./images/weapons/4A-weapon-mithril-sword.png)   |
| 0x4B  | Mithril Axe    | ![Mithril Axe](./images/weapons/4B-weapon-mithril-axe.png)       |
| 0x4C  | Broad Axe      | ![Broad Axe](./images/weapons/4C-weapon-broad-axe.png)           |
| 0x4D  | Hero Staff     | ![Hero Staff](./images/weapons/4D-weapon-hero-staff.png)         |
| 0x4E  | Heron Blade    | ![Heron Blade](./images/weapons/4E-weapon-heron-blade.png)       |
| 0x4F  | Death Axe      | ![Death Axe](./images/weapons/4F-weapon-death-axe.png)           |

| Value | Item             | Image                                                          |
| ----- | ---------------- | -------------------------------------------------------------- |
| 0x50  | Stealth Blade    | ![Stealth Blade](./images/weapons/50-weapon-stealth-blade.png) |
| 0x51  | Death Blade      | ![Death Blade](./images/weapons/51-weapon-death-blade.png)     |
| 0x52  | Arcane Sword     | ![Arcane Sword](./images/weapons/52-weapon-arcane-sword.png)   |
| 0x53  | Doom Blade       | ![Doom Blade](./images/weapons/53-weapon-doom-blade.png)       |
| 0x54  | Blood Axe        | ![Blood Axe](./images/weapons/54-weapon-blood-axe.png)         |
| 0x55  | Vorpal Blade     | ![Vorpal Blade](./images/weapons/55-weapon-vorpal-blade.png)   |
| 0x56  | Slugger          | ![Slugger](./images/weapons/56-weapon-slugger.png)             |
| 0x57  | Crystal Blade    | ![Crystal Blade](./images/weapons/57-weapon-crystal-blade.png) |
| 0x58  | Ancient Staff    | ![Ancient Staff](./images/weapons/58-weapon-ancient-staff.png) |
| 0x59  | Mystic Axe       | ![Mystic Axe](./images/weapons/59-weapon-mystic-axe.png)       |
| 0x5A  | Mystic Sword     | ![Mystic Sword](./images/weapons/5A-weapon-mystic-sword.png)   |
| 0x5B  | Mystic Dagger    | ![Mystic Dagger](./images/weapons/5B-weapon-mystic-dagger.png) |
| 0x5C  | Mystic Staff     | ![Mystic Staff](./images/weapons/5C-weapon-mystic-staff.png)   |
| 0x5D  | Bag of Gold      | ![Bag of Gold](./images/bags/5D-bag-gold.png)                  |
| 0x5E  | Bag of bat wings | ![Bag of bat wings](./images/bags/5E-bag-bat-wings.png)        |
| 0x5F  | Bag of sulphur   | ![Bag of sulphur](./images/bags/5F-bag-sulphur.png)            |

| Value | Item               | Image                                                       |
| ----- | ------------------ | ----------------------------------------------------------- |
| 0x60  | Bag of roots       | ![Bag of roots](./images/bags/60-bag-roots.png)             |
| 0x61  | Bag of crystals    | ![Bag of crystals](./images/bags/61-bag-crystals.png)       |
| 0x62  | Bag of venom sacs  | ![Bag of venom sacs](./images/bags/62-bag-venom-sacs.png)   |
| 0x63  | Bag of ugly teeth  | ![Bag of ugly teeth](./images/bags/63-bag-ugly-teeth.png)   |
| 0x64  | Bag of berries     | ![Bag of berries](./images/bags/64-bag-berries.png)         |
| 0x65  | Bag of fiery claws | ![Bag of fiery claws](./images/bags/65-bag-fiery-claws.png) |
| 0x66  | Amber Staff        | ![Amber Staff](./images/staves/66-staff-amber.png)          |
| 0x67  | Moon Staff         | ![Moon Staff](./images/staves/67-staff-moon.png)            |
| 0x68  | Cloud Staff        | ![Cloud Staff](./images/staves/68-staff-cloud.png)          |
| 0x69  | Serpent Staff      | ![Serpent Staff](./images/staves/69-staff-serpent.png)      |
| 0x6A  | Sun Staff          | ![Sun Staff](./images/staves/6A-staff-sun.png)              |
| 0x6B  | Dragon Staff       | ![Dragon Staff](./images/staves/6B-staff-dragon.png)        |
| 0x6C  | Amber Wand         | ![Amber Wand](./images/wands/6C-wand-amber.png)             |
| 0x6D  | Moon Wand          | ![Moon Wand](./images/wands/6D-wand-moon.png)               |
| 0x6E  | Cloud Wand         | ![Cloud Wand](./images/wands/6E-wand-cloud.png)             |
| 0x6F  | Dragon Wand        | ![Dragon Wand](./images/wands/6F-wand-dragon.png)           |

| Value | Item                                      | Image                                                                                                        |
| ----- | ----------------------------------------- | ------------------------------------------------------------------------------------------------------------ |
| 0x70  | Serpent Wand                              | ![Serpent Wand](./images/wands/70-wand-serpent.png)                                                          |
| 0x71  | Sun Wand                                  | ![Sun Wand](./images/wands/71-wand-sun.png)                                                                  |
| 0x72  | Crystal Wand                              | ![Crystal Wand](./images/wands/72-wand-crystal.png)                                                          |
| 0x73  | Scroll Heal (weak)                        | ![Scroll Heal (weak)](./images/scrolls/scroll-heal-weak.png)                                                 |
| 0x74  | Scroll Forward Dispell                    | ![Scroll Forward Dispell](./images/scrolls/scroll-forward-dispell.png)                                       |
| 0x75  | Scroll Surround Thrall                    | ![Scroll Surround Thrall](./images/scrolls/scroll-surround-thrall.png)                                       |
| 0x76  | Scroll Mystic Weapon                      | ![Scroll Mystic Weapon](./images/scrolls/scroll-make-weapon.png)                                             |
| 0x77  | Scroll Surround Paralyze                  | ![Scroll Surround Paralyze](./images/scrolls/scroll-surround-paralyze.png)                                   |
| 0x78  | Scroll Forward Paralyze                   | ![Scroll Forward Paralyze](./images/scrolls/scroll-forward-paralyze.png)                                     |
| 0x79  | Scroll Heal (medium)                      | ![Scroll Heal (medium](./images/scrolls/scroll-heal-medium.png)                                              |
| 0x7A  | Scroll Surround Damage                    | ![Scroll Surround Damage](./images/scrolls/scroll-surround-damage.png)                                       |
| 0x7B  | Hero Scroll                               | ![Hero Scroll](./images/scrolls/scroll-hero.png)                                                             |
| 0x7C  | Scroll Antimage Surround Antimage         | ![Scroll Antimage Surround Antimage](./images/scrolls/scroll-antimage-surround-antimage.png)                 |
| 0x7D  | Scroll Forward Damage                     | ![Scroll Forward Damage](./images/scrolls/scroll-forward-damage.png)                                         |
| 0x7E  | Scroll Heal (strong)                      | ![Scroll Heal (strong)](./images/scrolls/scroll-heal-strong.png)                                             |
| 0x7F  | Scroll Regeneration Surround Regeneration | ![Scroll Regeneration Surround Regeneration](./images/scrolls/scroll-regeneration-surround-regeneration.png) |

| Value | Item                      | Image                                                                        |
| ----- | ------------------------- | ---------------------------------------------------------------------------- |
| 0x80  | Scroll Forward Disrupt    | ![Scroll Disrupt](./images/scrolls/scroll-forward-disrupt.png)               |
| 0x81  | Scroll Heal Surround Heal | ![Scroll Heal Surround Heal](./images/scrolls/scroll-heal-surround-heal.png) |
| 0x82  | Scroll Forward Vivify     | ![Scroll Forward Vivify](./images/scrolls/scroll-forward-vivify.png)         |
| 0x83  | Craven Image              | ![Craven Image](./images/misc/83-misc-craven-image.png)                      |
| 0x84  | Holy Relic                | ![Holy Relic](./images/misc/84-misc-holy-relic.png)                          |
| 0x85  | Skull Key                 | ![Skull Key](./images/keys/85-key-skull.png)                                 |
| 0x86  | Iron Key                  | ![Iron Key](./images/keys/86-key-iron.png)                                   |
| 0x87  | Silver Key                | ![Silver Key](./images/keys/87-key-silver.png)                               |
| 0x88  | Bronze Key                | ![Bronze Key](./images/keys/88-key-bronze.png)                               |
| 0x89  | Gold Key                  | ![Gold Key](./images/keys/89-key-gold.png)                                   |
| 0x8A  | Diamond Key               | ![Diamond Key](./images/keys/8A-key-diamond.png)                             |
| 0x8B  | Crystal Key               | ![Crystal Key](./images/keys/8B-key-crystal.png)                             |
| 0x8C  | Azure Key                 | ![Azure Key](./images/keys/8C-key-azure.png)                                 |
| 0x8D  | Emerald Key               | ![Emerald Key](./images/keys/8D-key-emerald.png)                             |
| 0x8E  | Ruby Key                  | ![Ruby Key](./images/keys/8E-key-ruby.png)                                   |
| 0x8F  | Topaz Key                 | ![Topaz Key](./images/keys/8F-key-topaz.png)                                 |

| Value | Item                  | Image                                                                 |
| ----- | --------------------- | --------------------------------------------------------------------- |
| 0x90  | Ornate Key            | ![Ornate Key](./images/keys/90-key-ornate.png)                        |
| 0x91  | Dark Key              | ![Dark Key](./images/keys/91-key-dark.png)                            |
| 0x92  | Moon Key              | ![Moon Key](./images/keys/92-key-moon.png)                            |
| 0x93  | UnKey                 | ![UnKey](./images/keys/93-key-unkey.png)                              |
| 0x94  | Permit                | ![Permit](./images/misc/94-misc-permit.png)                           |
| 0x95  | Serpent Crystal       | ![Serpent Crystal](./images/misc/95-misc-crystal-serpent.png)         |
| 0x96  | Moon Crystal          | ![Moon Crystal](./images/misc/96-misc-crystal-moon.png)               |
| 0x97  | Dragon Crystal        | ![Dragon Crystal](./images/misc/97-misc-crystal-dragon.png)           |
| 0x98  | Chaos Crystal         | ![Chaos Crystal](./images/misc/98-misc-crystal-chaos.png)             |
| 0x99  | Slyzaar's Corpse      | ![Slyzaar's Corpse](./images/misc/99-misc-corpse-slyzaar.png)         |
| 0x9A  | Tunarle's Corpse      | ![Tunarle's Corpse](./images/misc/9A-misc-corpse-tunarle.png)         |
| 0x9B  | Thai-Chang's Corpse   | ![Thai-Chang's Corpse](./images/misc/9B-misc-corpse-thai-chang.png)   |
| 0x9C  | Zothen's Corpse       | ![Zothen's Corpse](./images/misc/9C-misc-corpse-zothen.png)           |
| 0x9D  | Serpent Potion (weak) | ![Serpent Potion (weak)](./images/potions/9D-potion-serpent-weak.png) |
| 0x9E  | Power Potion          | ![Power Potion](./images/potions/9E-potion-power.png)                 |
| 0x9F  | Cloud Potion          | ![Cloud Potion](./images/potions/9F-potion-cloud.png)                 |

| Value | Item                    | Image                                                                        |
| ----- | ----------------------- | ---------------------------------------------------------------------------- |
| 0xA0  | Moon Potion             | ![Moon Potion](./images/potions/A0-potion-moon.png)                          |
| 0xA1  | Amber Potion            | ![Amber Potion](./images/potions/A1-potion-amber.png)                        |
| 0xA2  | Serpent Potion (medium) | ![Serpent Potion (medium)](./images/potions/A2-potion-serpent-medium.png)    |
| 0xA3  | Hero Potion             | ![Hero Potion](./images/potions/A3-potion-hero.png)                          |
| 0xA4  | Golden Potion           | ![Golden Potion](./images/potions/A4-potion-golden.png)                      |
| 0xA5  | Serpent Potion (strong) | ![Serpent Potion (strong)](./images/potions/A5-potion-serpend-strong.png)    |
| 0xA6  | Bronze Potion           | ![Bronze Potion](./images/potions/A6-potion-bronze.png)                      |
| 0xA7  | Serpent Potion (max)    | ![Serpent Potion (max)](./images/potions/A7-potion-serpend-max.png)          |
| 0xA8  | Dragon Potion           | ![Dragon Potion](./images/potions/A8-potion-dragon.png)                      |
| 0xA9  | Horn                    | ![Horn](./images/instruments/A9-instrument-horn.png)                         |
| 0xAA  | Lyre                    | ![Lyre](./images/instruments/AA-instrument-lyre.png)                         |
| 0xAB  | Mandolin                | ![Mandolin](./images/instruments/AB-instrument-mandolin.png)                 |
| 0xAC  | Harmonic Lyre           | ![Harmonic Lyre](./images/instruments/AC-instrument-harmonic-lyre.png)       |
| 0xAD  | Crystal Mandolin        | ![Crystal Mandolin](./images/instruments/AD-instrument-crystal-mandolin.png) |
| 0xAE  | Diamond Horn            | ![Diamond Horn](./images/instruments/AE-instrument-diamond-horn.png)         |
| 0xAF  | Battle Horn             | ![Battle Horn](./images/instruments/AF-instrument-battle-horn.png)           |

| Value | Item            | Image                                                                      |
| ----- | --------------- | -------------------------------------------------------------------------- |
| 0xB0  | Arcane Mandolin | ![Arcane Mandolin](./images/instruments/B0-instrument-arcane-mandolin.png) |
| 0xB1  | Angel Harp      | ![Angel Harp](./images/instruments/B1-instrument-angel-harp.png)           |
| 0xB2  | Mixing Bowl     | ![Mixing Bowl](./images/misc/B2-misc-mixing-bowl.png)                      |

## Broken items

| Value | Item                             |
| ----- | -------------------------------- |
| 0xB3  | Dagger Dagger                    |
| 0xB4  | Dagger Dagger                    |
| 0xB5  | Dagger Dagger                    |
| 0xB6  | Dagger Dagger                    |
| 0xB7  | Surround Missile Disrupt Lyre    |
| 0xB8  | Tunarle's Gauntlets              |
| 0xB9  | Chain Chain                      |
| 0xBA  | Frost Blood                      |
| 0xBB  | Bane Golden                      |
| 0xBC  | Sword Chain                      |
| 0xBD  | Mithril Dagger (not really)      |
| 0xBE  | Dagger Dagger                    |
| 0xBF  | Mithril Surround Missile Disrupt |

| Value | Item |
| ----- | ---- |
| 0xC0  | -    |
| 0xC1  | -    |
| 0xC2  | -    |
| 0xC3  | -    |
| 0xC4  | -    |
| 0xC5  | -    |
| 0xC6  | -    |
| 0xC7  | -    |
| 0xC8  | -    |
| 0xC9  | -    |
| 0xCA  | -    |
| 0xCB  | -    |
| 0xCC  | -    |
| 0xCD  | -    |
| 0xCE  | -    |
| 0xCF  | -    |

| Value | Item |
| ----- | ---- |
| 0xD0  | -    |
| 0xD1  | -    |
| 0xD2  | -    |
| 0xD3  | -    |
| 0xD4  | -    |
| 0xD5  | -    |
| 0xD6  | -    |
| 0xD7  | -    |
| 0xD8  | -    |
| 0xD9  | -    |
| 0xDA  | -    |
| 0xDB  | -    |
| 0xDC  | -    |
| 0xDD  | -    |
| 0xDE  | -    |
| 0xDF  | -    |

| Value | Item |
| ----- | ---- |
| 0xE0  | -    |
| 0xE1  | -    |
| 0xE2  | -    |
| 0xE3  | -    |
| 0xE4  | -    |
| 0xE5  | -    |
| 0xE6  | -    |
| 0xE7  | -    |
| 0xE8  | -    |
| 0xE9  | -    |
| 0xEA  | -    |
| 0xEB  | -    |
| 0xEC  | -    |
| 0xED  | -    |
| 0xEE  | -    |
| 0xEF  | -    |

| Value | Item |
| ----- | ---- |
| 0xF0  | -    |
| 0xF1  | -    |
| 0xF2  | -    |
| 0xF3  | -    |
| 0xF4  | -    |
| 0xF5  | -    |
| 0xF6  | -    |
| 0xF7  | -    |
| 0xF8  | -    |
| 0xF9  | -    |
| 0xFA  | -    |
| 0xFB  | -    |
| 0xFC  | -    |
| 0xFD  | -    |
| 0xFE  | -    |
| 0xFF  | -    |
