# Troubadour

## Character offset

0x0084

## Item locations

| Offset      | Use                    |
| ----------- | ---------------------- |
| 0x00aa      | First item slot        |
| 0x00b9      | Last item slot         |
| 0x00c6-00c9 | Gold                   |
| 0x0288      | Bitmask of known songs |

## Songs

| Mask       | Song                      |
| ---------- | ------------------------- |
| 0b00000001 | The Thief of Dolik Pass   |
| 0b00000010 | Warriors Awaking          |
| 0b00000100 | The Ballad Eleanor        |
| 0b00001000 | Kiljam's Litany           |
| 0b00010000 | The Smithie Song          |
| 0b00100000 | March of the Bold Ones    |
| 0b01000000 | Adieu Sweet Dullard       |
| 0b10000000 | Dance of the Faerie Queen |
