# Locations

1 - Guild
2 - King
3 - Martindale
4 - Smathost

## 0x0281

Location screen you are currently on

| Value | Location       |
| ----- | -------------- |
| 0x00  | Leercote       |
| 0x01  | Zorendorf      |
| 0x02  | Delmortis      |
| 0x03  | Creyndor       |
| 0x04  | Martindale     |
| 0x00  | Hightower      |
| 0x06  | Smathost       |
| 0x07  | Treihadwyl     |
| 0x08  | Goriah         |
| 0x09  | Pongbarn       |
| 0x0A  | Hill's End     |
| 0x0B  | Eb's Pass      |
| 0x0C  | Groghurst      |
| 0x0D  | Balenhalm      |
| 0x0E  | Titanshal      |
| 0x0F  | Boot Hill      |
| 0x10  | Dak's Pond     |
| 0x11  | Brodfird       |
| 0x12  | The King       |
| 0x13  | Fagranc        |
| 0x14  | MoonHenge      |
| 0x15  | The Dark Tower |
| 0x16  | The Ancient    |
| 0x17  | The UnShrine   |
| 0x18  | The Guild      |
| 0x19  | Blacksmith     |
| 0x1A  | Apothecary     |
| 0x1B  | Holy Temple    |
| 0x1C  | Tavern         |
| 0x1D  | Artificer      |
| 0x1E  | Enemy Banner   |

## 0x0283

| Value | Options                                                        |
| ----- | -------------------------------------------------------------- |
| 0x00  | Get thee gone                                                  |
| 0x01  | The Ancient menu + broken Mindscape                            |
| 0x02  | You stand at the entrance to the city                          |
| 0x03  | Speak my name and enter                                        |
| 0x04  | Only knowledge of the true stone masters may invoke the stones |
| 0x05  | Blank screen                                                   |
| 0x06  | Blank screen                                                   |
| 0x07  | You may visit The Smithie or the Heron                         |
| 0x08  | You may visit The Smithie or the Heron                         |
| 0x09  | Blank screen                                                   |
| 0x0A  | Get thee gone                                                  |
| 0x0B  | Get thee gone                                                  |
| 0x0C  | Get thee gone                                                  |
| 0x0D  | Get thee gone                                                  |
| 0x0E  | Get thee gone                                                  |
| 0x0F  | Get thee gone                                                  |
| 0x10  | Get thee gone + king picture                                   |
| 0x11  | The Ancient menu + broken Mindscape                            |
| 0x12  | The Ancient menu + broken Mindscape                            |

## Map location

0x0390-0x0450

Something around there is your map location

0x000003C4 - 0x000003C7 - Current location of your flag
0x000003C8 - 0x000003CB - Location your flag is moving to

| Location       | Value       |
| -------------- | ----------- |
| Balenhalm      | 00 xx 00 xx |
| Boot Hill      | 00 49 00 82 |
| Brodfird       | 00 xx 00 xx |
| Creyndor       | 00 xx 00 xx |
| Dak's Pond     | 00 xx 00 xx |
| Delmortis      | 00 xx 00 xx |
| Eb's Pass      | 00 xx 00 xx |
| Fagranc        | 00 xx 00 xx |
| Goriah         | 00 xx 00 xx |
| Groghurst      | 00 xx 00 xx |
| Hightower      | 00 A7 00 44 |
| Hill's End     | 00 20 00 63 |
| Leercote       | 00 xx 00 xx |
| Martindale     | 00 7D 00 48 |
| MoonHenge      | 00 xx 00 xx |
| Pongbarn       | 00 xx 00 xx |
| Smathost       | 00 58 00 5D |
| The Ancient    | 00 xx 00 xx |
| The Dark Tower | 00 4A 00 3D |
| The King       | 00 8B 00 4F |
| The UnShrine   | 00 xx 00 xx |
| Titanshal      | 00 22 00 85 |
| Titanshal      | 00 xx 00 xx |
| Treihadwyl     | 00 98 00 5E |
| Zorendorf      | 00 xx 00 xx |

0x000003CC - 0x000003CF - Something to do with path finding

| Location       | Value       |
| -------------- | ----------- |
| Balenhalm      | 00 xx FF xx |
| Boot Hill      | 03 0F FF 0F |
| Brodfird       | 00 xx FF xx |
| Creyndor       | 00 xx FF xx |
| Dak's Pond     | 00 xx FF xx |
| Delmortis      | 00 xx FF xx |
| Eb's Pass      | 00 xx FF xx |
| Fagranc        | 00 xx FF xx |
| Goriah         | 00 xx FF xx |
| Groghurst      | 00 xx FF xx |
| Hightower      | 08 08 FF 08 |
| Hill's End     | 00 0C FF 0C |
| Leercote       | 00 xx FF xx |
| Martindale     | 05 08 FF 08 |
| MoonHenge      | 00 xx FF xx |
| Pongbarn       | 00 xx FF xx |
| Smathost       | 03 0B FF 0B |
| The Ancient    | 00 xx FF xx |
| The Dark Tower | 01 06 FF 06 |
| The King       | 06 09 FF 09 |
| The UnShrine   | 00 xx FF xx |
| Titanshal      | 00 xx FF xx |
| Titanshal      | 01 0F FF 0F |
| Treihadwyl     | 07 0B FF 0B |
| Zorendorf      | 00 xx FF xx |
