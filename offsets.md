# Offsets

Character start + offset

0x00 - east-west position on screen
0x01 - north-south position on screen
0x02 - move to position
0x03 - move to position

0x10 - Ac modifier
0x11 -
0x12 - Str
0x13 - Str repeat
0x14 - Int
0x15 - Int repeat
0x16 - Spd
0x17 - Spd repeat
0x18 - Dex
0x19 - Dex repeat
0x1A - Con
0x1B - Con repeat
0x1C - Luck
0x1D - Luck repeat
0x1E -

0x22 - remaining HP (high byte)
0x23 - remaining HP (low byte)
0x24 - total HP (high byte)
0x25 - total HP (low byte)

0x26 - item slot 1
0x27 - item slot 2
0x28 - item slot 3
0x29 - item slot 4
0x2A - item slot 5
0x2B - item slot 6
0x2C - item slot 7
0x2D - item slot 8
0x2E - item slot 9
0x2F - item slot 10
0x30 - item slot 11
0x31 - item slot 12
0x32 - item slot 13
0x33 - item slot 14
0x34 - item slot 15
0x35 - item slot 16

0x36 - slot of equipped helm
0x37 - slot of equipped ?
0x38 - slot of equipped footware
0x39 - slot of equipped shield
0x3A - slot of equipped armour
0x3B - slot of equipped gloves
0x3C - slot of equipped weapon
0x3D - slot of equipped ?
0x3E - slot of equipped off-hand item
0x3F - slot of selected item
0x40 - ?
0x41 - level

0x42 - gold
0x43 - gold
0x44 - gold
0x45 - gold

0x46 - xp
0x47 - xp
0x48 - xp
0x49 - xp

0x4A - Character type 0 = berserker, 2 = troubadour, 3 = assassin, 1 = runemaster

0x4B - ?

0x4C - bitmask toggle of starting elements 1 = Earth, 2 = Fire, 4 = Air, 8 = Water

Remaining seem to be zero padded
