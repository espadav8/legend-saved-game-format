# Berserker

## Character offset

0x0004

## Item locations

| Offset      | Use             |
| ----------- | --------------- |
| 0x002a      | First item slot |
| 0x0039      | Last item slot  |
| 0x0046-0049 | Gold            |
